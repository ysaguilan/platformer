﻿using UnityEngine;

using System.Collections;

public class Player : MonoBehaviour {
	public float maxSpeed = 9;
	public float speed;
	public float jumpPower;
	public bool faceRight;
	private bool grounded;

	public float jumpTime = 0;
	public float jumpDelay = .5f;

	private bool jumped;
	private int playerHealth = 50;
	private Rigidbody2D rigidBody;
	private Animator animate;
	private int healthPerHeart = 10;

	void Start () {
		animate = gameObject.GetComponent<Animator> ();
		rigidBody = gameObject.GetComponent<Rigidbody2D> ();
		/*playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Health: " + playerHealth;*/
	}
	
	void Update () {
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			faceRight = true;
			transform.localRotation = Quaternion.Euler(0, 0, 0);
			animate.SetTrigger("Walk");
			animate.SetTrigger("Land");

			//GetComponent<Ri
			//animate.SetFloat ("Speed", Mathf.Abs (GetComponent<Rigidbody2D> ().velocity.x));


		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			faceRight = false;
			transform.localRotation = Quaternion.Euler(0, 180, 0);
			animate.SetTrigger("Walk");
			animate.SetTrigger("Land");

			//GetComponent<Rigidbody2D> ().velocity = new Vector2 (-speed, 0);
			//animate.SetFloat ("Speed", Mathf.Abs (GetComponent<Rigidbody2D> ().velocity.x));

		}
	
		else {
			animate.SetFloat ("Speed", Mathf.Abs (GetComponent<Rigidbody2D> ().velocity.x));

		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, jumpPower);
			grounded = false;
			animate.SetTrigger("Jump");
		} 

	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith){
		if(objectPlayerCollidedWith.tag == "Flag"){

		}
		else if (objectPlayerCollidedWith.tag == "Heart"){
			playerHealth += healthPerHeart;
			Debug.Log("Heart up");
			//healthText.text = "+" + healthPerHeart + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
		}
	}
	void FixedUpdate(){

		float horizontal = Input.GetAxis("Horizontal");	
		rigidBody.AddForce((Vector2.right * speed) * horizontal);

		if(rigidBody.velocity.x > maxSpeed){
			rigidBody.velocity = new Vector2(maxSpeed, rigidBody.velocity.y);

		}
		if(rigidBody.velocity.x < -maxSpeed){
			rigidBody.velocity = new Vector2(-maxSpeed, rigidBody.velocity.y);

		}
		float vertical = Input.GetAxis("Vertical");	
		rigidBody.AddForce((Vector2.right * speed) * vertical);
	}
}